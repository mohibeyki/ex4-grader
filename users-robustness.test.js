/* Additional tests with shorter token expiration. */

const udb = `${__dirname}/testuserdatabase2.csv`;
const nconf = require('nconf');
nconf.defaults({
  userdatabase: udb,
  jwtsecret: 'test sauce',
  jwtexpirationtime: '1s'
});
const bcrypt = require('bcrypt');

// now that nconf defaults are set, we can require api which will use nconf defaults.
let api;

const request = require('supertest');

describe('Test User Database Deletion', function() {
  it('tests that the user database file can be reset', async () => {
    const userdb = require('./csvuserdatabase');
    let users = await userdb(udb);
    await users.clear();
    let password = await bcrypt.hash('adminpassword', 12);
    await users.add({
      name: 'admin',
      password,
      fullname: 'The Admin',
      admin: 1
    });

    api = require('../api'); // let api see the new user file
  });
});

describe('Test User Creation', function() {
  it(`tests that empty password is rejected`, async () => {
    const res = await request(api)
      .post('/api/users')
      .send({ username: 'bla', password: '' }) // should disallow empty password
      .expect(400);

    expect(res.body).toHaveProperty('message');
    expect(res.body.message.length).toBeGreaterThan(8);
  });

  /* It's probably safer to reject requests with extra properties */
  it(`tests that extra stuff password is rejected`, async () => {
    const res = await request(api)
      .post('/api/users')
      .send({
        username: 'bla',
        extrastuff: 'xyz',
        password: '',
        fullname: 'Fuller'
      })
      .expect(res => res.status - (res.status % 100) === 400);

    expect(res.body).toHaveProperty('message');
    expect(res.body.message.length).toBeGreaterThan(8);
  });
});

describe('Test Token Expiration', function() {
  let admintoken;

  it('that I can log on as administrator', async () => {
    let res = await request(api)
      .post(`/api/login`)
      .send({ username: 'admin', password: 'adminpassword' })
      .expect(200);
    expect(res.body).toHaveProperty('token');
    admintoken = res.body.token;
  });

  it('tests that an admin can list users', async () => {
    let res = await request(api)
      .get(`/api/users`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect(200);
    const { users: rusers } = res.body;
    expect(rusers).toContainEqual({ name: 'admin', id: 0 });
  });

  it('tests that admin token has expired', done => {
    setTimeout(() => {
      request(api)
        .get(`/api/users`)
        .set('Authorization', `Bearer ${admintoken}`)
        .expect(401, done);
    }, 1000);
  });
});
