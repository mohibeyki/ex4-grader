/* Unit tests for API stage 2 */

// file backing the user database for inspection and help with debugging
const udb = `${__dirname}/testuserdatabase.csv`;
const nconf = require('nconf');
nconf.defaults({
  userdatabase: udb,
  jwtsecret: 'test sauce',
  jwtexpirationtime: '5m'
});
const bcrypt = require('bcrypt');

let api;

const request = require('supertest');

/*
 * Generally, we would want specs to be independent so that the testing framework
 * can execute them in random order.  Here, however, I am relying on jest
 * executing them in order as per
 * https://jestjs.io/docs/en/setup-teardown#order-of-execution-of-describe-and-test-blocks
 */
describe('Test User Database Deletion', function() {
  it('tests that the user database file can be reset', async () => {
    const userdb = require('./csvuserdatabase');
    let users = await userdb(udb);
    await users.clear();
    let password = await bcrypt.hash('adminpassword', 12);
    await users.add({
      name: 'admin',
      password,
      fullname: 'The Admin',
      admin: 1
    });

    api = require('../api'); // let api see the new user file
  });
});

describe('Basic API Tests', function() {
  it('checks that the API is up', async () => {
    const res = await request(api)
      .get('/api')
      .expect('Content-Type', /json/)
      .expect(200);
    const response = res.body;
    expect(response.message).toBe('API is accessible');
  });
});

// http://listofrandomnames.com
const users = [
  'Brinda Breau',
  'Stephania Sollers',
  'Kourtney Keels',
  'Tamatha Tiggs',
  'Melanie Montijo',
  'Roselyn Relf',
  'Eugenio Eppinger',
  'Racheal Reeves',
  'Tomeka Townsley',
  'Brittani Breeden',
  'Cuc Cavallo',
  'Santana Stolz',
  'Josiah Jerkins',
  'Jerrold Jolin',
  'Leigh Langner',
  'Sunday Shulman',
  'Aimee Asay',
  'Jessie Jun',
  'Gerard Gayhart',
  'Xiomara Xiong',
  'Tijuana Tsao',
  'Lucilla Longtin',
  'Madaline Mayson',
  'Charisse Casey',
  'Genia Goldfarb',
  'Doreatha Dampier',
  'Daniella Devoe',
  'Dyan Duquette',
  'Anne Allensworth',
  'Delena Dinan'
].map(fullname => {
  let [first, last] = fullname.split(/\s+/);
  return {
    name: first.toLowerCase().charAt(0) + last.toLowerCase(),
    password: last
      .split('')
      .reverse()
      .join(''),
    fullname
  };
});

describe('Test User Creation', function() {
  it.each(users)(`tests that user %j can be created`, async ut => {
    const res = await request(api)
      .post('/api/users')
      .send(ut)
      .expect(200);
    expect(res.body).toHaveProperty('token');
    expect(res.body).toHaveProperty('user');
    const { token, user, message, ...rest } = res.body;
    expect(message.length).toBeGreaterThan(8);
    expect(rest).toEqual({});
    expect(user.username).toBe(ut.name);
    expect(user.fullname).toBe(ut.fullname);
    expect(user).not.toHaveProperty('password');
    ut.id = user.id;
    ut.token = token;
  });
  it(`tests that attempts to create incomplete users are rejected`, async () => {
    return request(api)
      .post('/api/users')
      .send({ fullname: 'Mr. NoName & NoPassword' })
      .expect(400); // incomplete request
  });

  it(`tests that attempts to create users w/o password are rejected`, async () => {
    return request(api)
      .post('/api/users')
      .send({ fullname: 'Mrs. NoPassword', name: 'mrsnopassword' })
      .expect(400); // incomplete request
  });
});

describe('Test User Creation', function() {
  const { id, ...usersansid } = users[0]; // eslint-disable-line no-unused-vars
  it(`tests that existing users can't be created again with the same name`, async () => {
    await request(api)
      .post('/api/users')
      .send(usersansid)
      .expect(409);
  });
});

describe('Test User Login', function() {
  it(`tests that system treats wrong users and passwords the same`, async () => {
    let res = await request(api)
      .post(`/api/login`)
      .send({ username: 'admin', password: 'nottheadminpassword' })
      .expect(401);
    expect(res.body).toHaveProperty('message');
    let msg_wrong_password = res.body.message;
    let res2 = await request(api)
      .post(`/api/login`)
      .send({ username: 'thisuserdoesnotexist', password: 'does not matter' })
      .expect(401);
    expect(res2.body).toHaveProperty('message');
    expect(res2.body.message).toBe(msg_wrong_password);
  });
});

let admintoken;
describe('Test Post User Creation', function() {
  it.each(users)(`tests that user %j can be retrieved`, async user => {
    let res = await request(api)
      .get(`/api/users/${user.id}`)
      .set('Authorization', `Bearer ${user.token}`)
      .expect(200)
      .expect('Content-Type', /json/);

    const ruser = res.body;
    expect(ruser.name).toEqual(user.name);
    expect(ruser.fullname).toEqual(user.fullname);
    expect(ruser).not.toHaveProperty('password');
  });

  it('that I can log on as administrator', async () => {
    let res = await request(api)
      .post(`/api/login`)
      .send({ username: 'admin', password: 'adminpassword' })
      .expect(200);
    expect(res.body).toHaveProperty('token');
    admintoken = res.body.token;
  });

  it('tests that an admin can list users', async () => {
    let res = await request(api)
      .get(`/api/users`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect(200);
    const { users: rusers } = res.body;
    for (const { name, id } of users) {
      expect(rusers).toContainEqual({ name, id });
    }
  });

  it('tests that an unauthorized user cannot list users', async () => {
    return request(api)
      .get(`/api/users`)
      .expect(401);
  });

  it('tests that an authorized non-admin user cannot list users', async () => {
    return request(api)
      .get(`/api/users`)
      .set('Authorization', `Bearer ${users[0].token}`)
      .expect(403);
  });

  let usera = users[0];
  it('tests that an unauthorized user cannot delete users', async () => {
    return request(api)
      .delete(`/api/users/${usera.id}`)
      .expect(401);
  });

  it('tests that a user can delete themselves when authorized', async () => {
    await request(api)
      .delete(`/api/users/${usera.id}`)
      .set('Authorization', `Bearer ${usera.token}`)
      .expect(200);
    return request(api)
      .get(`/api/users/${usera.id}`)
      .set('Authorization', `Bearer ${usera.token}`)
      .expect(404); // and subsequently cannot be found
  });

  let userb = users[1];
  it('tests that an admin can delete a user', async () => {
    await request(api)
      .delete(`/api/users/${userb.id}`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect(200);
    return request(api)
      .get(`/api/users/${userb.id}`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect(404); // and subsequently cannot be found
  });

  let userc = users[2];
  it("tests that a user's fullname and password can be updated", async () => {
    return Promise.all(
      [admintoken, userc.token].map(async token => {
        await request(api)
          .put(`/api/users/${userc.id}`)
          .set('Authorization', `Bearer ${token}`)
          .send({ fullname: 'Stephania Kellogg', password: 'new password' })
          .expect(200);
        let res = await request(api)
          .get(`/api/users/${userc.id}`)
          .set('Authorization', `Bearer ${token}`)
          .expect(200);
        const ruser = res.body;
        expect(ruser.fullname).toBe('Stephania Kellogg');
        userc.fullname = 'Stephania Kellogg';
        userc.password = 'new password';
      })
    );
  });

  it('tests that a user cannot be updated w/o authentication', async () => {
    await request(api)
      .put(`/api/users/${userc.id}`)
      .send({ fullname: 'Stephania Kellogg', password: 'new password1' })
      .expect(401);
  });

  it('tests that a user cannot be updated w/o authorization', async () => {
    await request(api)
      .put(`/api/users/${userc.id}`)
      .set('Authorization', `Bearer ${userb.token}`)
      .send({ fullname: 'Stephania Kellogg', password: 'new password2' })
      .expect(403);
  });
});

describe('Test Post User Creation', function() {
  const byName = {};
  users.forEach(user => (byName[user.name] = user));

  it('tests that users can be listed', async () => {
    let res = await request(api)
      .get(`/api/users`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect('Content-Type', /json/)
      .expect(200);
    const { users: rusers } = res.body;
    const justName = ({ name }) => name;
    const have = new Set(rusers.map(justName));
    const shouldBe = new Set(
      users
        .slice(2) // first two were deleted
        .reverse()
        .map(justName)
    );
    shouldBe.add('admin');
    expect(have).toEqual(shouldBe);
    return Promise.all(
      rusers.map(async ruser => {
        const token =
          ruser.username in byName ? byName[ruser.username].token : admintoken;
        const {
          body: { name, id, fullname }
        } = await request(api)
          .get(`/api/users/${ruser.id}`)
          .set('Authorization', `Bearer ${token}`)
          .expect('Content-Type', /json/)
          .expect(200);
        expect(name).toBe(ruser.name);
        expect(id).toBe(ruser.id);
        if (token !== admintoken) expect(fullname).toBe(byName[name].fullname);
      })
    );
  });

  it('checks that all passwords are encrypted properly', async () => {
    const userdb = require('./csvuserdatabase');
    let usersinfile = await userdb(udb);
    return Promise.all(
      users.slice(2).map(async user => {
        let storeduser = usersinfile.getByName(user.name);
        expect(usersinfile.byName.has(user.name)).toBeTruthy();
        expect(
          await bcrypt.compare(user.password, storeduser.password)
        ).toBeTruthy();
      })
    );
  });
});
