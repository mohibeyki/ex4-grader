import csv

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import figure

count = 0

pids = []
grades = []

with open('ex4-grades.csv', mode='r') as csv_file:
    reader = csv.DictReader(csv_file)
    for row in reader:
        count += 1
        pids.append(row['PID'])
        grades.append(float(row['ex4']))

x = np.array(grades) * 100
num_bins = 10
n, bins, patches = plt.hist(
    x,
    num_bins,
    range=(0, 100),
    cumulative=False,
    density=0,
    facecolor='blue',
    alpha=1
)

plt.title(r'Exercise 4 Grades')
plt.xlabel('Grade')
plt.ylabel('Number of Students')
plt.subplots_adjust(left=0.15)
figure(figsize=(20, 6))
plt.show()
