import csv
import glob
import os
import subprocess
import sys

from details import tests, passed

cwd = os.getcwd() + '/'
exercise_name = ''


def init_submissions():
    files = glob.glob(cwd + 'uploads/*-' + exercise_name + '*.zip')
    submissions = {}
    for file in files:
        end = file.find('-' + exercise_name)
        start = file[:end].rfind('/')
        pid = file[start + 1:end]
        if pid not in submissions:
            submissions[pid] = file
        else:
            ver = (file[file.rfind('-') + 1:-3])
            prev_ver = (submissions[pid][submissions[pid].rfind('-') + 1:-3])
            if ver > prev_ver:
                submissions[pid] = file
    return submissions


def cleanup(pid, submissions, dir_name):
    subprocess.run(['rm', '-rf', cwd + 'prj'])
    subprocess.run(['mkdir', '-p', cwd + 'prj'])
    subprocess.run('rm -rf ' + cwd + 'temp/*', shell=True)

    subprocess.run(['mkdir', '-p', dir_name])
    subprocess.run(['unzip', '-uo', submissions[pid], '-d', 'temp'], stdout=subprocess.DEVNULL)


def get_score_for_line(line):
    for test, weight in tests.items():
        if line[2:].strip().startswith(test):
            if line[0] == passed:
                return weight
    return 0


def commit_csv(grades, names):
    with open(exercise_name + '-grades.csv', mode='w') as csv_file:
        fieldnames = ['pid', exercise_name + '-grade']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        for index, grade in enumerate(grades):
            writer.writerow({'pid': names[index], exercise_name + '-grade': grade})


def get_project_dir():
    for root, dirs, files in os.walk("temp"):
        for file in files:
            if file.endswith("package.json"):
                return root
    return None


def main():
    grades = []
    names = []
    count = 0
    submissions = init_submissions()
    for pid in sorted(submissions):
        dirname = cwd + 'grades/' + pid + '/' + exercise_name
        cleanup(pid, submissions, dirname)

        linter_problems = 0
        project_dir = get_project_dir()
        passes = 0
        if project_dir is not None:
            subprocess.run('ln -sf ' + cwd + 'node_modules ' + cwd + 'prj/node_modules', shell=True)
            subprocess.run('cp -rf ' + cwd + project_dir + '/* ' + cwd + 'prj/', shell=True)
            subprocess.run('cp -f  ' + cwd + 'users-with-auth.test.js ' + cwd + 'prj/api/', shell=True)
            subprocess.run('cp -f  ' + cwd + 'users-robustness.test.js ' + cwd + 'prj/api/', shell=True)
            subprocess.run('cp -f  ' + cwd + 'package.json ' + cwd + 'prj/', shell=True)

            process = subprocess.Popen(
                'gtimeout 20 npm run test',
                cwd=cwd + 'prj',
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )

            _, stderr = process.communicate()
            lines = stderr.decode("utf-8").split('\n')

            feedback_file = dirname + '/' + exercise_name + '-feedback.txt'
            with open(feedback_file, 'w') as feedback:
                for line in lines:
                    feedback.write(line + '\n')
                    passes += get_score_for_line(line.strip())

            linter_output = subprocess.run('npm run --prefix ' + cwd + 'prj checklintrules', shell=True,
                                           stderr=subprocess.DEVNULL, stdout=subprocess.PIPE)

            lines = linter_output.stdout.decode("utf-8").split('\n')
            with open(feedback_file, 'a') as feedback:
                for line in lines:
                    feedback.write(line + '\n')
                    if line.strip().startswith('✖'):
                        linter_problems = int(line.split()[1])

        if linter_problems >= 10:
            passes = passes * 0.9
        elif linter_problems > 1:
            passes = passes * 0.95

        grade = float(passes) / 780.0

        grades.append(grade)
        names.append(pid)
        count += 1
        print(count, 'of', len(submissions), pid, 'passes:', str(passes) + '/780', 'linter problems:', linter_problems,
              '\t\tDEBUG\t\t', submissions[pid])

        commit_csv(grades, names)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage python grader.py "exercise-name"')
        exit(1)

    exercise_name = sys.argv[1]
    print('grading', exercise_name)
    main()
